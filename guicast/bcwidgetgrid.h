#ifndef BCWIDGETGRID_H
#define BCWIDGETGRID_H

#include "arraylist.h"
#include "bcrelocatablewidget.h"
#include "bctoggle.h"
#include "bctextbox.h"
#include "bcsubwindow.h"

class grid_widgetinfo {
public:
	int                    wtype;
	int                    valign;
	int                    halign;
	int                    colspan;
	int                    rowspan;
	BC_RelocatableWidget   *wh;
};

class grid_rowinfo {
public:
	int minh;
	int maxh;
};

class grid_columninfo {
public:
	int minw;
	int maxw;
};

class BC_WidgetGrid : public BC_RelocatableWidget {
public:  
	BC_WidgetGrid(int x, int y, int x_r, int y_b, int colgaps, int rowgaps);
	~BC_WidgetGrid();

	BC_RelocatableWidget * add(BC_RelocatableWidget *h, int row, int col);
	void calculate_maxs();
	void clear_widget(int row, int col);

	int  get_x();
	int  get_y();

	int  get_h();
	int  get_h_wm();
	int  get_w();
	int  get_w_wm();

	int  getc_w(int col, int recalculate = 0);
	int  getr_h(int row, int recalculate = 0);

	int  getw_w(int row, int col);
	int  getw_h(int row, int col);

	int  guess_x(int col);
	int  guess_y(int row);

	void move_widgets();
	void print();
	int  reposition_widget(int x, int y, int w = -1, int h = -1);

	void set_align(int r,int c,int va, int ha);
	void set_crspan(int r,int c,int cs, int rs);
	void set_minh(int c, int h);
	void set_minw(int c, int w);
	void setw_position(int row, int col, int x, int y);

	enum {
		VALIGN_TOP,
		VALIGN_CENTER,
		VALIGN_BOTTOM
	};

	enum {
		HALIGN_LEFT,
		HALIGN_CENTER,
		HALIGN_RIGHT
	};

	enum {
		BC_WT_NONE,
		BC_WT_RelocatableWidget
	};

private:
	void growgrid(int row, int col);

	int rowcount, colcount;

	grid_widgetinfo *wginf;
	grid_widgetinfo *wgdefault;

	grid_columninfo *colinf;
	grid_rowinfo    *rowinf;

	int rowgaps;
	int colgaps;

	int x_l, x_r, y_t, y_b; // left, right, top,bottom margins.
};


class BC_WidgetGridList : public ArrayList<BC_WidgetGrid*>
{
public:
	BC_WidgetGridList();
	~BC_WidgetGridList();
};

#endif
/*
//      Local Variables:
//      mode: C++
//      c-file-style: "linux"
//      End:
*/
