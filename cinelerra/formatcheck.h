#ifndef FORMATCHECK_H
#define FORMATCHECK_H

#include "assets.inc"

class FormatCheck
{
public:
	FormatCheck(Asset_GC asset);
	~FormatCheck();

	int check_format();

private:
	Asset_GC asset;
};


#endif
/*
//      Local Variables:
//      mode: C++
//      c-file-style: "linux"
//      End:
*/
