#ifndef ASSETS_H
#define ASSETS_H

#include <stdio.h>
#include <stdint.h>

#include "arraylist.h"
#include "asset.inc"
#include "assets.inc"
#include "batch.inc"
#include "bchash.inc"
#include "edl.inc"
#include "filexml.inc"
#include "guicast.h"
#include "pluginserver.inc"
#include "sharedlocation.h"

#include <list>

class Assets
{
        Assets_list assets;
public:
        // expose some lists interface with inline functions
        Assets_list::size_type size()
                { return assets.size();}
        Assets_list::iterator begin()
                { return assets.begin();}
        Assets_list::iterator end()
                { return assets.end();}
        Assets_list::const_iterator begin() const
                { return assets.begin();}
        Assets_list::const_iterator end() const
                { return assets.end();}

	Assets(EDL *edl);
	virtual ~Assets();

	int load(ArrayList<PluginServer*> *plugindb, 
		FileXML *xml, 
		uint32_t load_flags);
	int save(ArrayList<PluginServer*> *plugindb, 
		FileXML *xml, 
		char *output_path);
	Assets& operator=(const Assets &assets);
	void copy_from(const Assets &assets);

// Enter a new asset into the table.
// If the asset already exists return the asset which exists.
// If the asset doesn't exist, store a copy of the argument and return the copy.
	Asset_GC update(Asset_GC asset);
// Update the index information for assets with the same path
	void update_index(Asset &asset);


// Parent EDL
	EDL *edl;
	

	int delete_all();
	int dump();

// return the asset containing this path or create a new asset containing this path
	Asset_GC update(const char *path);

// Insert the asset into the list if it doesn't exist already but
// don't create a new asset.
	void update_ptr(Asset_GC asset);

// return the asset containing this path or 0 if not found
	Asset_GC get_asset(const char *filename);
// remove asset from table
	void remove_asset(Asset_GC asset);

// return number of the asset
	int number_of(Asset_GC asset);
	Asset_GC asset_number(int number);

	int update_old_filename(char *old_filename, char *new_filename);
};




#endif
/*
//      Local Variables:
//      mode: C++
//      c-file-style: "linux"
//      End:
*/
