#ifndef MASKAUTO_INC
#define MASKAUTO_INC



class MaskAuto;
class MaskPoint;
class SubMask;

enum
{
	MASK_MULTIPLY_ALPHA,
	MASK_SUBTRACT_ALPHA
};

#define SUBMASKS 8



#endif
/*
//      Local Variables:
//      mode: C++
//      c-file-style: "linux"
//      End:
*/
