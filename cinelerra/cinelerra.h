#ifndef CINELERRA_H
#define CINELERRA_H

#include "config.h"
#include "nobug.h"

/* TODO: undef NoBug macros which collide with cinelerra macros for now
   later the cinelerra macros should be fixed replaced with their NoBug
   counterparts, this requires support from NoBug for application defined
   logging which I already planned and will be implemented soon
*/
#undef TRACE
#undef ERROR
#undef WARN

#endif /* CINELERRA_H */
/*
//      Local Variables:
//      mode: C++
//      c-file-style: "linux"
//      End:
*/
