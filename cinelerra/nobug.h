/*
    nobug.h - a small debugging library

  Copyright (C) 2006, 2007, Christian Thaeter <ct@pipapo.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/
#ifndef NOBUG_H
#define NOBUG_H

#ifdef NDEBUG
#ifdef EBUG_ALPHA
#error NDEBUG incompatible with -DEBUG_ALPHA
#endif
#ifdef EBUG_BETA
#error NDEBUG incompatible with -DEBUG_BETA
#endif
#endif

#if defined(EBUG_ALPHA)
#       define NOBUG_MODE_ALPHA 1
#       define NOBUG_MODE_BETA 0
#       define NOBUG_MODE_RELEASE 0
#elif defined(EBUG_BETA)
#       define NOBUG_MODE_ALPHA 0
#       define NOBUG_MODE_BETA 1
#       define NOBUG_MODE_RELEASE 0
#elif defined(NDEBUG)
#       define NOBUG_MODE_ALPHA 0
#       define NOBUG_MODE_BETA 0
#       define NOBUG_MODE_RELEASE 1
#else
#error no debug level and no NDEBUG defined
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <syslog.h>

#ifdef HAVE_EXECINFO_H
# ifndef NOBUG_USE_EXECINFO
#  define NOBUG_USE_EXECINFO 1
# endif
#else
# undef NOBUG_USE_EXECINFO
# define NOBUG_USE_EXECINFO 0
#endif

#if NOBUG_USE_EXECINFO
#include <execinfo.h>
#endif

#if defined(HAVE_VALGRIND_VALGRIND_H) && !defined(NVALGRIND)
# ifndef NOBUG_USE_VALGRIND
#  define NOBUG_USE_VALGRIND 1
# endif
#else
# undef NOBUG_USE_VALGRIND
# define NOBUG_USE_VALGRIND 0
#endif

#if NOBUG_USE_VALGRIND
#include <valgrind/valgrind.h>
#endif


/*
  todo ringbuffer bugreport thingx
  include functionname in logging output
*/

#if NOBUG_MODE_RELEASE==0
#undef assert
#define assert(expr) NOBUG_ASSERT(expr)
#endif

//TODO better logging target handling, more than one
#ifndef NOBUG_LOGGING_TARGET
//IF alpha or beta
#define NOBUG_LOGGING_TARGET NOBUG_TARGET_STDERR
//IF release
//#define NOBUG_LOGGING_TARGET NOBUG_TARGET_SYSLOG
#endif

/*
  check preconditions (incoming parameters)
*/

#define NOBUG_REQUIRE(expr, ...)                                                \
NOBUG_IF_NOT_RELEASE(NOBUG_ASSERT_(expr, "PRECONDITION",                        \
                                   __FILE__, __LINE__, ## __VA_ARGS__))


#define NOBUG_REQUIRE_DBG(expr, ...)                                            \
NOBUG_IF_NOT_RELEASE(NOBUG_ASSERT_DBG_(expr, "PRECONDITION",                    \
                                       __FILE__, __LINE__, ## __VA_ARGS__))


#define NOBUG_REQUIRE_IF(when, expr, ...) \
NOBUG_WHEN(when, NOBUG_REQUIRE(expr, ## __VA_ARGS__))

#define NOBUG_REQUIRE_IF_DBG(when, expr, ...) \
NOBUG_WHEN(when, NOBUG_REQUIRE_DBG(expr, ## __VA_ARGS__))

/*
  check postcondistions (computation outcomes)
*/
#define NOBUG_ENSURE(expr, ...)                                         \
NOBUG_MODE_SWITCH(                                                      \
          NOBUG_ASSERT_(expr, "POSTCONDITION",                          \
                        __FILE__, __LINE__, ## __VA_ARGS__),            \
          NOBUG_BLOCK (                                                 \
            if (NOBUG_SCOPE_UNCHECKED)                                  \
              {                                                         \
                NOBUG_ASSERT_(expr, "POSTCONDITION",                    \
                              __FILE__, __LINE__, ## __VA_ARGS__);      \
              }                                                         \
          ),                                                            \
          NOBUG_PASS                                                    \
)

#define NOBUG_ENSURE_DBG(expr, ...)                                             \
NOBUG_MODE_SWITCH(                                                              \
                NOBUG_ASSERT_DBG_(expr, "POSTCONDITION",                        \
                                  __FILE__, __LINE__, ## __VA_ARGS__),          \
                NOBUG_BLOCK (                                                   \
                  if (NOBUG_SCOPE_UNCHECKED)                                    \
                    {                                                           \
                      NOBUG_ASSERT_DBG_(expr, "POSTCONDITION",                  \
                                        __FILE__, __LINE__, ## __VA_ARGS__);    \
                    }                                                           \
                ),                                                              \
                NOBUG_PASS                                                      \
)

#define NOBUG_ENSURE_IF(when, expr, ...) \
NOBUG_WHEN(when, NOBUG_ENSURE(expr, ## __VA_ARGS__))

#define NOBUG_ENSURE_IF_DBG(when, expr, ...) \
NOBUG_WHEN(when, NOBUG_ENSURE_DBG(expr, ## __VA_ARGS__))


/*
  generic assertion
*/

#define NOBUG_ASSERT(expr, ...)                                                         \
NOBUG_IF_NOT_RELEASE(                                                                   \
          NOBUG_BLOCK (                                                                 \
          NOBUG_ASSERT_(expr, "ASSERTION", __FILE__, __LINE__, ## __VA_ARGS__)          \
          ))

#define NOBUG_ASSERT_DBG(expr, ...)                                     \
NOBUG_IF_NOT_RELEASE(                                                   \
          NOBUG_BLOCK (                                                 \
            NOBUG_ASSERT_DBG_(expr, "ASSERTION",                        \
                              __FILE__, __LINE__, ## __VA_ARGS__);      \
          } while(0)                                                    \
)

#define NOBUG_ASSERT_IF(when, expr, ...) \
NOBUG_WHEN(when, NOBUG_ASSERT(expr, ## __VA_ARGS__))

#define NOBUG_ASSERT_IF_DBG(when, expr, ...) \
NOBUG_WHEN(when, NOBUG_ASSERT_DBG(expr, ## __VA_ARGS__))

#define NOBUG_ASSERT_(expr, what, file, line, ...)                              \
if (NOBUG_EXPECT_FALSE(!(expr)))                                                \
  {                                                                             \
    NOBUG_LOG_( NOBUG_ON, LOG_EMERG,                                            \
                NOBUG_BASENAME(file), line, what, __func__, "(%s) "             \
                NOBUG_HEAD(__VA_ARGS__),                                        \
                #expr NOBUG_TAIL(__VA_ARGS__));                                 \
    NOBUG_BACKTRACE;                                                            \
    NOBUG_ABORT;                                                                \
  }

#define NOBUG_ASSERT_DBG_(expr, what, file, line, ...)                          \
NOBUG_IF_DBG_ACTIVE(                                                            \
if (NOBUG_EXPECT_FALSE(!(expr)))                                                \
{                                                                               \
    NOBUG_LOG_( NOBUG_ON, LOG_EMERG,                                            \
                NOBUG_BASENAME(file), line, what, __func__, "(%s) "             \
                NOBUG_HEAD(__VA_ARGS__),                                        \
                #expr NOBUG_TAIL(__VA_ARGS__));                                 \
  NOBUG_BACKTRACE;                                                              \
  NOBUG_ABORT;                                                                  \
})


/*
  invariant check
*/

#define NOBUG_INVARIANT(type, pointer, depth)                                   \
NOBUG_MODE_SWITCH(                                                              \
          NOBUG_BLOCK (                                                         \
            if (NOBUG_SCOPE_UNCHECKED)                                          \
              {                                                                 \
                NOBUG_CAT(type,_invariant)(pointer, depth, __FILE__,            \
                          __LINE__, __func__);                                  \
              }                                                                 \
          ),                                                                    \
          NOBUG_PASS, NOBUG_PASS                                                \
)

#define NOBUG_INVARIANT_DBG(type, pointer, ...)                                 \
NOBUG_MODE_SWITCH(                                                              \
          NOBUG_IF_DBG_ACTIVE (                                                 \
            if (NOBUG_SCOPE_UNCHECKED)                                          \
              {                                                                 \
                NOBUG_CAT(type,_invariant)(pointer, depth, __FILE__, __LINE__); \
              }                                                                 \
          ),                                                                    \
          NOBUG_PASS, NOBUG_PASS                                                \
)

#define NOBUG_INVARIANT_IF(when, type, pointer, ...) \
NOBUG_WHEN(when, NOBUG_INVARIANT(type, pointer, ## __VA_ARGS__))

#define NOBUG_INVARIANT_IF_DBG(when, type, pointer, ...) \
NOBUG_WHEN(when, NOBUG_INVARIANT_DBG(type, pointer, ## __VA_ARGS__))

#define NOBUG_INVARIANT_ASSERT(expr, ...)                               \
 NOBUG_ASSERT_(expr, "INVARIANT", file, line, ## __VA_ARGS__)


/*
  dumping datastructures
*/

#define NOBUG_DUMP(flag, type, pointer, depth)                          \
NOBUG_IF_NOT_RELEASE(                                                   \
NOBUG_BLOCK(                                                            \
if (NOBUG_EXPECT_FALSE (NOBUG_DUMP_LEVEL <= nobug_flag_##flag))         \
{                                                                       \
          NOBUG_BLOCK (                                                 \
            NOBUG_CAT3(nobug_, type, _dump)(pointer, depth,             \
                                            __FILE__, __LINE__,         \
                                            __func__);                  \
          );                                                            \
}))

#define NOBUG_DUMP_DBG(flag, type, pointer, depth)                      \
NOBUG_IF_NOT_RELEASE(                                                   \
NOBUG_BLOCK(                                                            \
          NOBUG_IF_DBG_ACTIVE (                                         \
if (NOBUG_EXPECT_FALSE(NOBUG_DUMP_LEVEL <= nobug_flag_##flag))          \
{                                                                       \
            NOBUG_CAT3(nobug_, type, _dump)(pointer, depth,             \
                                            __FILE__, __LINE__,         \
                                            __func__);                  \
}          )                                                            \
))

#define NOBUG_DUMP_IF(expr, flag, type, pointer, depth)                 \
NOBUG_BLOCK (                                                           \
if (NOBUG_EXPECT_FALSE(NOBUG_DUMP_LEVEL <= nobug_flag_##flag))          \
{                                                                       \
            if (NOBUG_EXPECT_FALSE(expr))                               \
              {                                                         \
                NOBUG_CAT3(nobug_, type,_dump)(pointer, depth,          \
                                               __FILE__, __LINE__,      \
                                               __func__);               \
              }                                                         \
})

#define NOBUG_DUMP_IF_DBG(expr, flag, type, pointer, depth)             \
NOBUG_IF_NOT_RELEASE(                                                   \
          NOBUG_IF_DBG_ACTIVE (                                         \
if (NOBUG_EXPECT_FALSE(NOBUG_DUMP_LEVEL <= nobug_flag_##flag))          \
{                                                                       \
            if (NOBUG_EXPECT_FALSE(expr))                               \
              {                                                         \
                NOBUG_CAT(nobug_, type,_dump)(pointer, depth,           \
                                              __FILE__, __LINE__,       \
                                              __func__);                \
              }                                                         \
}))

#ifndef NOBUG_DUMP_LEVEL
#define NOBUG_DUMP_LEVEL LOG_DEBUG
#endif

#define NOBUG_DUMP_LOG(fmt, ...)                                \
    NOBUG_LOG_( NOBUG_ON, NOBUG_DUMP_LEVEL,                     \
                NOBUG_BASENAME(file), line, "DUMP", func,       \
                fmt, ## __VA_ARGS__)                            \


#define NOBUG_DUMP_LOG_DBG(fmt, ...)                            \
NOBUG_IF_DBG_ACTIVE (                                           \
    NOBUG_LOG_( NOBUG_ON, NOBUG_DUMP_LEVEL,                     \
                NOBUG_BASENAME(file), line, "DUMP", func,       \
                fmt, ## __VA_ARGS__)                            \
)

#define NOBUG_DUMP_LOG_IF(expr, fmt, ...)                       \
NOBUG_BLOCK (                                                   \
  if (NOBUG_EXPECT_FALSE(expr)) {                               \
    NOBUG_LOG_( NOBUG_ON, NOBUG_DUMP_LEVEL,                     \
                NOBUG_BASENAME(file), line, "DUMP", func,       \
                fmt, ## __VA_ARGS__)                            \
  }                                                             \
)

#define NOBUG_DUMP_LOG_IF_DBG(expr, fmt, ...)                   \
NOBUG_IF_DBG_ACTIVE (                                           \
  if (NOBUG_EXPECT_FALSE(expr)) {                               \
    NOBUG_LOG_( NOBUG_ON, NOBUG_DUMP_LEVEL,                     \
                NOBUG_BASENAME(file), line, "DUMP", func,       \
                fmt, ## __VA_ARGS__)                            \
  }                                                             \
)


/*
  Logging
*/

#define NOBUG_LOG(flag, lvl, ...)                               \
NOBUG_BLOCK (                                                   \
  NOBUG_LOG_(flag, lvl, NOBUG_BASENAME(__FILE__), __LINE__,     \
    NOBUG_LVL(lvl), __func__, ## __VA_ARGS__);                  \
)

#define NOBUG_LOG_DBG(flag, lvl, ...)                           \
NOBUG_IF_DBG_ACTIVE (                                           \
  NOBUG_LOG_(flag, lvl, NOBUG_BASENAME(__FILE__), __LINE__,     \
    NOBUG_LVL(lvl), __func__, ## __VA_ARGS__);                  \
)

#define NOBUG_LOG_IF(expr, flag, lvl, ...)                      \
NOBUG_BLOCK (                                                   \
  if (NOBUG_EXPECT_FALSE(expr)) {                               \
    NOBUG_LOG_(flag, lvl, NOBUG_BASENAME(__FILE__), __LINE__,   \
      NOBUG_LVL(lvl), __func__, ## __VA_ARGS__);                \
  }                                                             \
)

#define NOBUG_LOG_IF_DBG(expr, flag, lvl, ...)                  \
NOBUG_IF_DBG_ACTIVE (                                           \
  if (NOBUG_EXPECT_FALSE(expr)) {                               \
    NOBUG_LOG_(flag, lvl, NOBUG_BASENAME(__FILE__), __LINE__,   \
      NOBUG_LVL(lvl), __func__, ## __VA_ARGS__);                \
  }                                                             \
)

/*
  low level logging handler
*/
#define NOBUG_LOG_(flag, lvl, file, line, what, func, ...)      \
if (NOBUG_EXPECT_FALSE(lvl <= nobug_flag_##flag))                \
{                                                                       \
NOBUG_MODE_SWITCH (                                                     \
  if (NOBUG_EXPECT_FALSE(NOBUG_DBG_ACTIVE)) {                           \
    NOBUG_LOG_2(lvl, file, line, what, func, "" __VA_ARGS__);   \
  } else {                                                              \
    NOBUG_CAT(NOBUG_LOG_, NOBUG_LOGGING_TARGET)                         \
                (lvl, file, line, what, func, "" __VA_ARGS__);     \
  },                                                                    \
  NOBUG_CAT(NOBUG_LOG_, NOBUG_LOGGING_TARGET)                           \
                (lvl, file, line, what, func, "" __VA_ARGS__);     \
  ,                                                                     \
  NOBUG_CAT(NOBUG_LOG_, NOBUG_LOGGING_TARGET)                           \
                (lvl, file, line, what, func, "" __VA_ARGS__);     \
)}

#define NOBUG_LOG_0(lvl, file, line, what, func, fmt, ...) \
  fprintf(stderr, "%s:%d: debug: %s: %s: "fmt"\n", file, line, what, func, ## __VA_ARGS__)

#define NOBUG_LOG_1(lvl, file, line, what, func, fmt, ...) \
  syslog(lvl, "%s:%d: %s: %s: "fmt, file, line, what, func, ## __VA_ARGS__)

#define NOBUG_LOG_2(lvl, file, line, what, func, fmt, ...)                              \
NOBUG_IF(NOBUG_USE_VALGRIND,                                                            \
  VALGRIND_PRINTF("%s:%d: debug: %s: %s: "fmt, file, line, what, func, ## __VA_ARGS__)) \
NOBUG_IFNOT(NOBUG_USE_VALGRIND,                                                         \
  NOBUG_LOG_0(lvl, file, line, what, func, fmt, ## __VA_ARGS__))

#define NOBUG_LOG_3(lvl, file, line, what, func, fmt, ...) \
  fprintf(stderr, "%s:%d: debug: %s: %s: "fmt"\n", file, line, what, func, ## __VA_ARGS__)

#define NOBUG_LVL(lvl) NOBUG_LVL_##lvl
#define NOBUG_LVL_0 "EMERG"
#define NOBUG_LVL_1 "ALERT"
#define NOBUG_LVL_2 "CRIT"
#define NOBUG_LVL_3 "ERR"
#define NOBUG_LVL_4 "WARNING"
#define NOBUG_LVL_5 "NOTICE"
#define NOBUG_LVL_6 "INFO"
#define NOBUG_LVL_7 "TRACE"

#define NOBUG_TARGET_STDERR 0
#define NOBUG_TARGET_SYSLOG 1
#define NOBUG_TARGET_DEBUGGER 2
#define NOBUG_TARGET_FILE 3

#define NOBUG_ERROR(flag, ...)                     \
NOBUG_LOG(flag, LOG_ERR, ## __VA_ARGS__)
#define NOBUG_ERROR_IF(expr, flag, ...)             \
NOBUG_LOG_IF(expr, flag, LOG_ERR, ## __VA_ARGS__)
#define NOBUG_ERROR_DBG(flag, ...)                     \
NOBUG_LOG_DBG(flag, LOG_ERR, ## __VA_ARGS__)
#define NOBUG_ERROR_IF_DBG(expr, flag, ...)             \
NOBUG_LOG_IF_DBG(expr, flag, LOG_ERR, ## __VA_ARGS__)

#define NOBUG_WARN(flag, ...)                      \
NOBUG_LOG(flag, LOG_WARNING, ## __VA_ARGS__)
#define NOBUG_WARN_IF(expr, flag, ...)              \
NOBUG_LOG_IF(expr, flag, LOG_WARNING, ## __VA_ARGS__)
#define NOBUG_WARN_DBG(flag, ...)                      \
NOBUG_LOG_DBG(flag, LOG_WARNING, ## __VA_ARGS__)
#define NOBUG_WARN_IF_DBG(expr, flag, ...)              \
NOBUG_LOG_IF_DBG(expr, flag, LOG_WARNING, ## __VA_ARGS__)

#define NOBUG_INFO(flag, ...)                    \
NOBUG_LOG(flag, LOG_INFO, ## __VA_ARGS__)
#define NOBUG_INFO_IF(expr, flag, ...)              \
NOBUG_LOG_IF(expr, flag, LOG_INFO, ## __VA_ARGS__)
#define NOBUG_INFO_DBG(flag, ...)                    \
NOBUG_LOG_DBG(flag, LOG_INFO, ## __VA_ARGS__)
#define NOBUG_INFO_IF_DBG(expr, flag, ...)              \
NOBUG_LOG_IF_DBG(expr, flag, LOG_INFO, ## __VA_ARGS__)

#define NOBUG_NOTICE(flag, ...)                    \
NOBUG_LOG(flag, LOG_NOTICE, ## __VA_ARGS__)
#define NOBUG_NOTICE_IF(expr, flag, ...)            \
NOBUG_LOG_IF(expr, flag, LOG_NOTICE, ## __VA_ARGS__)
#define NOBUG_NOTICE_DBG(flag, ...)                    \
NOBUG_LOG_DBG(flag, LOG_NOTICE, ## __VA_ARGS__)
#define NOBUG_NOTICE_IF_DBG(expr, flag, ...)            \
NOBUG_LOG_IF_DBG(expr, flag, LOG_NOTICE, ## __VA_ARGS__)

#define NOBUG_TRACE(flag, ...)                     \
NOBUG_LOG(flag, LOG_DEBUG, ## __VA_ARGS__)
#define NOBUG_TRACE_IF(expr, flag, ...)             \
NOBUG_LOG_IF(expr, flag, LOG_DEBUG, ## __VA_ARGS__)
#define NOBUG_TRACE_DBG(flag, ...)                     \
NOBUG_LOG_DBG(flag, LOG_DEBUG, ## __VA_ARGS__)
#define NOBUG_TRACE_IF_DBG(expr, flag, ...)             \
NOBUG_LOG_IF_DBG(expr, flag, LOG_DEBUG, ## __VA_ARGS__)

#define NOBUG_BASENAME(name) (strrchr((name), '/')?strrchr((name), '/') + 1 : (name))

#define NOBUG_SCOPE_UNCHECKED NOBUG_CHECKED_VALUE == 0

#define NOBUG_DBG_ACTIVE                                \
NOBUG_IF(NOBUG_USE_VALGRIND, (RUNNING_ON_VALGRIND?2:0)) \
NOBUG_IFNOT(NOBUG_USE_VALGRIND, (0))

/*
             alpha                                              beta                            release
unchecked    Preconditions, Postconditions, Invariants          Preconditions, Postconditions   compiling will abort
checked      Preconditions, Postconditions                      Preconditions
*/

#define CHECKED NOBUG_IF_NOT_RELEASE(enum {NOBUG_CHECKED_VALUE=1})

#define UNCHECKED                                                                       \
	NOBUG_IF_NOT_RELEASE(enum {NOBUG_CHECKED_VALUE=0})                              \
	NOBUG_IF(NOBUG_MODE_RELEASE, NOBUG_UNCHECKED_NOT_ALLOWED_IN_RELEASE_BUILD)

#define NOBUG_ABORT abort()

#define NOBUG_DBG_GDB 1
#define NOBUG_DBG_VALGRIND 2

#if 0 // no gdb support yet
  case NOBUG_DBG_GDB:                           \
    NOBUG_BACKTRACE_GDB;                        \
    break;                                      \

#endif

#define NOBUG_BACKTRACE                         \
NOBUG_MODE_SWITCH(                              \
  switch (NOBUG_DBG_ACTIVE) {                   \
  case NOBUG_DBG_VALGRIND:                      \
    NOBUG_BACKTRACE_VALGRIND;                   \
    break;                                      \
  default:                                      \
    NOBUG_BACKTRACE_GLIBC;                      \
  },                                            \
  NOBUG_BACKTRACE_GLIBC,                        \
  NOBUG_BACKTRACE_GLIBC                         \
)

#define NOBUG_BACKTRACE_DBG                     \
NOBUG_IF_NOT_RELEASE(                           \
  switch (NOBUG_DBG_ACTIVE) {                   \
  case NOBUG_DBG_VALGRIND:                      \
    NOBUG_BACKTRACE_VALGRIND;                   \
    break;                                      \
  })

#define NOBUG_BACKTRACE_GDB fprintf(stderr, "UNIMPLEMENTED : GDB Backtraces\n")

#define NOBUG_BACKTRACE_VALGRIND                \
NOBUG_IF(NOBUG_USE_VALGRIND,                    \
VALGRIND_PRINTF_BACKTRACE("BACKTRACE : %s@%d",  \
  NOBUG_BASENAME(__FILE__), __LINE__))


#ifndef NOBUG_BACKTRACE_DEPTH
#define NOBUG_BACKTRACE_DEPTH 256
#endif

#define NOBUG_BACKTRACE_GLIBC                                                           \
NOBUG_IF_NOT_RELEASE(                                                                   \
NOBUG_BLOCK (                                                                           \
NOBUG_IF(NOBUG_USE_EXECINFO,                                                            \
  void* nobug_backtrace_buffer[NOBUG_BACKTRACE_DEPTH];                                  \
  backtrace_symbols_fd (nobug_backtrace_buffer,                                         \
                        backtrace (nobug_backtrace_buffer, NOBUG_BACKTRACE_DEPTH), 2))))

/*
                alpha           beta            release
UNIMPLEMENTED   abort           abort           wont compile
*/
#define NOBUG_UNIMPLEMENTED(msg)                                                        \
NOBUG_IF_NOT_RELEASE (                                                                  \
         NOBUG_BLOCK (                                                                  \
         NOBUG_LOG_ (NOBUG_ON, LOG_EMERG, NOBUG_BASENAME(__FILE__),                     \
                     __LINE__, "UNIMPLEMENTED", __func__, msg);                         \
         NOBUG_ABORT;                                                                   \
         ))                                                                             \
NOBUG_IF(NOBUG_MODE_RELEASE, NOBUG_UNIMPLEMENTED_NOT_ALLOWED_IN_RELEASE_BUILD(msg);)

/*
                alpha           beta            release
FIXME           log             wont compile    wont compile
*/
#define NOBUG_FIXME(msg)                                                \
NOBUG_MODE_SWITCH(                                                      \
         NOBUG_ONCE (                                                   \
         NOBUG_LOG_ (NOBUG_ON, LOG_ALERT, NOBUG_BASENAME(__FILE__),     \
                     __LINE__, "FIXME", __func__, msg);                 \
         ),                                                             \
         NOBUG_FIXME_NOT_ALLOWED_IN_RELEASE_BUILD(msg),                 \
         NOBUG_FIXME_NOT_ALLOWED_IN_RELEASE_BUILD(msg)                  \
)

/*
                alpha           beta            release
TODO            log             log             wont compile
*/
#define NOBUG_TODO(msg)                                                         \
NOBUG_IF_NOT_RELEASE (                                                          \
         NOBUG_ONCE (                                                           \
         NOBUG_LOG_( NOBUG_ON, LOG_CRIT, NOBUG_BASENAME(__FILE__), __LINE__,    \
                    "TODO", __func__, msg);                                     \
         ))                                                                     \
NOBUG_IF(NOBUG_MODE_RELEASE, NOBUG_TODO_NOT_ALLOWED_IN_RELEASE_BUILD(msg);)

/*
                alpha           beta            release
PLANNED         log             nothing         nothing
*/
#define NOBUG_PLANNED(msg)                                              \
NOBUG_MODE_SWITCH (                                                     \
         NOBUG_ONCE (                                                   \
         NOBUG_LOG_ (NOBUG_ON, LOG_INFO, NOBUG_BASENAME(__FILE__),      \
                    __LINE__, "PLANNED", __func__, msg);                \
         ),                                                             \
         NOBUG_PASS,NOBUG_PASS                                          \
)

/*
                alpha           beta            release
NOTREACHED      abort           abort           nothing
*/
#define NOBUG_NOTREACHED                                                \
NOBUG_IF_NOT_RELEASE (                                                  \
         NOBUG_BLOCK (                                                  \
         NOBUG_LOG_( NOBUG_ON, LOG_EMERG, NOBUG_BASENAME(__FILE__),     \
                      __LINE__, "PLANNED", __func__);                   \
         NOBUG_ABORT;                                                   \
         )                                                              \
)                                                                       \
NOBUG_IF(NOBUG_MODE_RELEASE, NOBUG_PASS)

#ifdef __GNUC__
#define NOBUG_CLEANUP(fn)  __attribute__((cleanup(fn)))
#else
#define NOBUG_CLEANUP(fn)
#endif

#define NOBUG_ONCE(code)                                        \
NOBUG_BLOCK (                                                   \
  static int NOBUG_CAT(nobug_once_,__LINE__) = 0;               \
  if (NOBUG_EXPECT_FALSE(!NOBUG_CAT(nobug_once_,__LINE__)))     \
    {                                                           \
      NOBUG_CAT(nobug_once_,__LINE__) = 1;                      \
      code;                                                     \
    }                                                           \
)

#define NOBUG_PASS ;

#define NOBUG_BLOCK(code) do { code; } while (0)

// branch prediction on bools
#if __GNUC__
#define NOBUG_EXPECT_FALSE(x) __builtin_expect(!!(x),0)
#else
#define NOBUG_EXPECT_FALSE(x) x
#endif

#define NOBUG_IF_DBG_ACTIVE(code)                       \
NOBUG_IF(NOBUG_MODE_ALPHA,                              \
         NOBUG_BLOCK (                                  \
           if (NOBUG_EXPECT_FALSE(NOBUG_DBG_ACTIVE))    \
             {                                          \
               code;                                    \
             }                                          \
         )                                              \
)                                                       \
NOBUG_IF(NOBUG_NOT(NOBUG_MODE_ALPHA), NOBUG_PASS)

#define NOBUG_WHEN(when, code) \
NOBUG_IF_NOT_RELEASE(do{ if (when){code;}}while(0))

#define NOBUG_IF_NOT_RELEASE(code)              \
NOBUG_IF(NOBUG_NOT(NOBUG_MODE_RELEASE), code)   \
NOBUG_IF(NOBUG_MODE_RELEASE, NOBUG_PASS)

#define NOBUG_MODE_SWITCH(alpha_code, beta_code, release_code)  \
NOBUG_IF(NOBUG_MODE_ALPHA, alpha_code)                          \
NOBUG_IF(NOBUG_MODE_BETA, beta_code)                            \
NOBUG_IF(NOBUG_MODE_RELEASE, release_code)

#define NOBUG_IF(bool, code) NOBUG_CAT(NOBUG_IF_,bool)(code)
#define NOBUG_IF_1(code) code
#define NOBUG_IF_0(_)

#define NOBUG_IFNOT(bool, code) NOBUG_CAT(NOBUG_IF_, NOBUG_NOT(bool))(code)

#define NOBUG_NOT(bool) NOBUG_CAT(NOBUG_NOT_, bool)
#define NOBUG_NOT_1 0
#define NOBUG_NOT_0 1

#define NOBUG_AND(a,b) NOBUG_CAT3(NOBUG_AND_, a, b)
#define NOBUG_AND_00 0
#define NOBUG_AND_01 0
#define NOBUG_AND_10 0
#define NOBUG_AND_11 1

#define NOBUG_OR(a,b) NOBUG_CAT3(NOBUG_OR_, a, b)
#define NOBUG_OR_00 0
#define NOBUG_OR_01 1
#define NOBUG_OR_10 1
#define NOBUG_OR_11 1

#define NOBUG_XOR(a,b) NOBUG_CAT( NOBUG_XOR_, NOBUG_CAT(a,b))
#define NOBUG_XOR_00 0
#define NOBUG_XOR_01 1
#define NOBUG_XOR_10 1
#define NOBUG_XOR_11 0

#define NOBUG_CAT(a,b) NOBUG_CAT_(a,b)
#define NOBUG_CAT_(a,b) a##b

#define NOBUG_CAT3(a,b,c) NOBUG_CAT3_(a,b,c)
#define NOBUG_CAT3_(a,b,c) a##b##c

#define NOBUG_HEAD(head, ...) head
#define NOBUG_TAIL(_, ...) , ## __VA_ARGS__

#define NOBUG_STRINGIZE(s) NOBUG_STRINGIZE_(s)
#define NOBUG_STRINGIZE_(s) #s


#define NOBUG_LIMIT_TABLE "\0"                  \
NOBUG_STRINGIZE(LOG_EMERG)"\tEMERG\n"           \
NOBUG_STRINGIZE(LOG_ALERT)"\tALERT\n"           \
NOBUG_STRINGIZE(LOG_CRIT)"\tCRIT\n"             \
NOBUG_STRINGIZE(LOG_ERR)"\tERR\n"               \
NOBUG_STRINGIZE(LOG_WARNING)"\tWARNING\n"       \
NOBUG_STRINGIZE(LOG_NOTICE)"\tNOTICE\n"         \
NOBUG_STRINGIZE(LOG_INFO)"\tINFO\n"             \
NOBUG_STRINGIZE(LOG_DEBUG)"\tDEBUG\n"           \
NOBUG_STRINGIZE(LOG_DEBUG)"\tTRACE\n"

/*
  logging is enabled by setting a enviroment variable NOBUG_LOG to contain the flag,
  note that this is just a simple substr search, this can have spurious or
  intended side effects.
  example:

  NOBUG_LOG='Memory,IO' ./mytestprogram
*/

#define NOBUG_FLAG(name) NOBUG_CAT(nobug_flag_,name)

#define NOBUG_DEFINE_FLAG(name) int NOBUG_FLAG(name) = -1
#define NOBUG_DEFINE_FLAG_LIMIT(name, limit) int NOBUG_FLAG(name) = (limit)
#define NOBUG_DECLARE_FLAG(name) extern int NOBUG_FLAG(name)

#define NOBUG_INIT_FLAG(name)                                   \
NOBUG_FLAG(name) = nobug_env_get_flag (#name, NOBUG_FLAG(name));

#ifndef NOBUG_LOG_LIMIT_ALPHA
#       define NOBUG_LOG_LIMIT_ALPHA LOG_INFO
#endif
#ifndef NOBUG_LOG_LIMIT_BETA
#       define NOBUG_LOG_LIMIT_BETA LOG_WARNING
#endif
#ifndef NOBUG_LOG_LIMIT_RELEASE
#       define NOBUG_LOG_LIMIT_RELEASE LOG_CRIT
#endif

#ifndef NOBUG_LOG_LIMIT
#       define NOBUG_LOG_LIMIT                  \
NOBUG_MODE_SWITCH(                              \
    NOBUG_LOG_LIMIT_ALPHA,                      \
    NOBUG_LOG_LIMIT_BETA,                       \
    NOBUG_LOG_LIMIT_RELEASE)
#endif

#define nobug_flag_NOBUG_ON LOG_DEBUG

#define NOBUG_SET_LIMIT(flag, min)      \
NOBUG_MODE_SWITCH(                      \
  NOBUG_FLAG(flag) = (min),             \
  NOBUG_FLAG(flag) = (min),             \
  NOBUG_PASS)

static inline int
nobug_env_get_limit ()
{
  const char* lstr = getenv("NOBUG_LIMIT");

  if (lstr)
    {
      const char* s = strstr (NOBUG_LIMIT_TABLE+1, lstr);
      if (s && *(s-1) == '\t')
        return *(s-2) - '0';
    }
  return NOBUG_LOG_LIMIT;
}

static inline int
nobug_env_get_flag (const char* name, int old_value)
{
  static int env_limit = -1;
  if (env_limit == -1)
    env_limit = nobug_env_get_limit ();

  const char* lstr = getenv ("NOBUG_LOG");
  const char* flag = NULL;
  char buf[8];
  const char* s;

  if (lstr)
    flag = strstr(lstr, name);

  if (flag)
    switch (flag[strlen (name)])
      {
      case ':':
        /* followed by :LIMIT */
        strncpy (buf, flag + strlen (name) +1, 7);
        buf[7] = '\0';
        buf[strspn (buf, "ABCDEFGHIJKLMNOPQRSTUVWXYZ")] = '\0';
        s = strstr (NOBUG_LIMIT_TABLE+1, buf);
        if (s && *(s-1) == '\t')
          return *(s-2) - '0';
        break;
      case ',':
      case '\0':
        /* use default */
        return env_limit;
      }
  return old_value;
}

NOBUG_IF_NOT_RELEASE(UNCHECKED);

/*
  short macros without NOBUG_
*/

#ifndef REQUIRE
#define REQUIRE NOBUG_REQUIRE
#endif
#ifndef REQUIRE_DBG
#define REQUIRE_DBG NOBUG_REQUIRE_DBG
#endif
#ifndef REQUIRE_IF
#define REQUIRE_IF NOBUG_REQUIRE_IF
#endif
#ifndef REQUIRE_IF_DBG
#define REQUIRE_IF_DBG NOBUG_REQUIRE_IF_DBG
#endif
#ifndef ENSURE
#define ENSURE NOBUG_ENSURE
#endif
#ifndef ENSURE_DBG
#define ENSURE_DBG NOBUG_ENSURE_DBG
#endif
#ifndef ENSURE_IF
#define ENSURE_IF NOBUG_ENSURE_IF
#endif
#ifndef ENSURE_IF_DBG
#define ENSURE_IF_DBG NOBUG_ENSURE_IF_DBG
#endif
#ifndef ASSERT
#define ASSERT NOBUG_ASSERT
#endif
#ifndef ASSERT_DBG
#define ASSERT_DBG NOBUG_ASSERT_DBG
#endif
#ifndef ASSERT_IF
#define ASSERT_IF NOBUG_ASSERT_IF
#endif
#ifndef ASSERT_IF_DBG
#define ASSERT_IF_DBG NOBUG_ASSERT_IF_DBG
#endif
#ifndef INVARIANT
#define INVARIANT NOBUG_INVARIANT
#endif
#ifndef INVARIANT_DBG
#define INVARIANT_DBG NOBUG_INVARIANT_DBG
#endif
#ifndef INVARIANT_IF
#define INVARIANT_IF NOBUG_INVARIANT_IF
#endif
#ifndef INVARIANT_IF_DBG
#define INVARIANT_IF_DBG NOBUG_INVARIANT_IF_DBG
#endif
#ifndef INVARIANT_ASSERT
#define INVARIANT_ASSERT NOBUG_INVARIANT_ASSERT
#endif
#ifndef DUMP
#define DUMP NOBUG_DUMP
#endif
#ifndef DUMP_DBG
#define DUMP_DBG NOBUG_DUMP_DBG
#endif
#ifndef DUMP_IF
#define DUMP_IF NOBUG_DUMP_IF
#endif
#ifndef DUMP_IF_DBG
#define DUMP_IF_DBG NOBUG_DUMP_IF_DBG
#endif
#ifndef DUMP_LOG
#define DUMP_LOG NOBUG_DUMP_LOG
#endif
#ifndef DUMP_LOG_DBG
#define DUMP_LOG_DBG NOBUG_DUMP_LOG_DBG
#endif
#ifndef DUMP_LOG_IF
#define DUMP_LOG_IF NOBUG_DUMP_LOG_IF
#endif
#ifndef DUMP_LOG_IF_DBG
#define DUMP_LOG_IF_DBG NOBUG_DUMP_LOG_IF_DBG
#endif
#ifndef LOG
#define LOG NOBUG_LOG
#endif
#ifndef LOG_DBG
#define LOG_DBG NOBUG_LOG_DBG
#endif
#ifndef LOG_IF
#define LOG_IF NOBUG_LOG_IF
#endif
#ifndef LOG_IF_DBG
#define LOG_IF_DBG NOBUG_LOG_IF_DBG
#endif
#ifndef ERROR
#define ERROR NOBUG_ERROR
#endif
#ifndef ERROR_DBG
#define ERROR_DBG NOBUG_ERROR_DBG
#endif
#ifndef ERROR_IF
#define ERROR_IF NOBUG_ERROR_IF
#endif
#ifndef ERROR_IF_DBG
#define ERROR_IF_DBG NOBUG_ERROR_IF_DBG
#endif
#ifndef WARN
#define WARN NOBUG_WARN
#endif
#ifndef WARN_DBG
#define WARN_DBG NOBUG_WARN_DBG
#endif
#ifndef WARN_IF
#define WARN_IF NOBUG_WARN_IF
#endif
#ifndef WARN_IF_DBG
#define WARN_IF_DBG NOBUG_WARN_IF_DBG
#endif
#ifndef INFO
#define INFO NOBUG_INFO
#endif
#ifndef INFO_DBG
#define INFO_DBG NOBUG_INFO_DBG
#endif
#ifndef INFO_IF
#define INFO_IF NOBUG_INFO_IF
#endif
#ifndef INFO_IF_DBG
#define INFO_IF_DBG NOBUG_INFO_IF_DBG
#endif
#ifndef NOTICE
#define NOTICE NOBUG_NOTICE
#endif
#ifndef NOTICE_DBG
#define NOTICE_DBG NOBUG_NOTICE_DBG
#endif
#ifndef NOTICE_IF
#define NOTICE_IF NOBUG_NOTICE_IF
#endif
#ifndef NOTICE_IF_DBG
#define NOTICE_IF_DBG NOBUG_NOTICE_IF_DBG
#endif
#ifndef TRACE
#define TRACE NOBUG_TRACE
#endif
#ifndef TRACE_DBG
#define TRACE_DBG NOBUG_TRACE_DBG
#endif
#ifndef TRACE_IF
#define TRACE_IF NOBUG_TRACE_IF
#endif
#ifndef TRACE_IF_DBG
#define TRACE_IF_DBG NOBUG_TRACE_IF_DBG
#endif
#ifndef BACKTRACE
#define BACKTRACE NOBUG_BACKTRACE
#endif
#ifndef BACKTRACE_DBG
#define BACKTRACE_DBG NOBUG_BACKTRACE_DBG
#endif
#ifndef UNIMPLEMENTED
#define UNIMPLEMENTED NOBUG_UNIMPLEMENTED
#endif
#ifndef FIXME
#define FIXME NOBUG_FIXME
#endif
#ifndef TODO
#define TODO NOBUG_TODO
#endif
#ifndef PLANNED
#define PLANNED NOBUG_PLANNED
#endif
#ifndef NOTREACHED
#define NOTREACHED NOBUG_NOTREACHED
#endif
#ifndef CLEANUP
#define CLEANUP NOBUG_CLEANUP
#endif

#endif
/*
//      Local Variables:
//      mode: C++
//      c-file-style: "linux"
//      End:
*/
