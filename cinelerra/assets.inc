#ifndef ASSETS_INC
#define ASSETS_INC


class Assets;


// Index statuses
#define INDEX_READY     0
#define INDEX_NOTTESTED 1
#define INDEX_BUILDING  2
#define INDEX_TOOSMALL  3

#include "asset.inc"

#include <list>
#include <vector>
#include <boost/shared_ptr.hpp>

typedef std::list<Asset_GC> Assets_list;
typedef std::vector<Asset_GC> Assets_vector;

typedef boost::shared_ptr<Assets> Assets_GC;


#endif
/*
//      Local Variables:
//      mode: C++
//      c-file-style: "linux"
//      End:
*/
