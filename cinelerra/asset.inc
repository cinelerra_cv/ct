#ifndef ASSET_INC
#define ASSET_INC


class Asset;
class InterlaceautofixoptionItem;
class InterlacefixmethodItem;

#include <boost/shared_ptr.hpp>

typedef boost::shared_ptr<Asset> Asset_GC;

#endif
/*
//      Local Variables:
//      mode: C++
//      c-file-style: "linux"
//      End:
*/
