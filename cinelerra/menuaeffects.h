#ifndef MENUAEFFECTS_H
#define MENUAEFFECTS_H

#include "asset.inc"
#include "edl.inc"
#include "guicast.h"
#include "mwindow.inc"
#include "menueffects.h"
#include "pluginserver.inc"

class MenuAEffects : public MenuEffects
{
public:
	MenuAEffects(MWindow *mwindow);
	~MenuAEffects();
};

class MenuAEffectThread : public MenuEffectThread
{
public:
	MenuAEffectThread(MWindow *mwindow);
	~MenuAEffectThread();

	int get_recordable_tracks(Asset_GC asset);
	int get_derived_attributes(Asset_GC asset, BC_Hash *defaults);
	int save_derived_attributes(Asset_GC asset, BC_Hash *defaults);
	PluginArray* create_plugin_array();
	int64_t to_units(double position, int round);
	int fix_menu(char *title);
};


class MenuAEffectItem : public MenuEffectItem
{
public:
	MenuAEffectItem(MenuAEffects *menueffect, char *string);
};




#endif
/*
//      Local Variables:
//      mode: C++
//      c-file-style: "linux"
//      End:
*/
