#!/bin/bash

#
# This script was used to covert recursive Makefile.am's to included versions in the plugins dir
# it does only some stupid repeating tasks, manual editing is needed afterwards

echo "This was a one-shot script and is only kept for documentation purposes"
exit

cd plugins

cat Makefile.am |
while read file; do
    # process commented out include lines
    if test "${file:0:8}" = "#include"; then
        makefileam=${file#*/*/}
        pluginname=$(sed -e 's/noinst_LTLIBRARIES *= *\(.*\).la/\1/p;d' $makefileam)
        if test ! "$pluginname"; then
            pluginname=${makefileam%/*}
        fi

        echo "Working in $makefileam for $pluginname"

        #cp $makefileam $makefileam.new

        echo "Editing"
        ed $makefileam <<EOF
H
# Formatting, souround = with spaces, remove double spaces and tabs and spaces at end of line
g/=/s/=/ = /
g/  /s/  / /g
g/ *$/s/ *$//
#
# insert the srcdir var
1i
${pluginname}_la_srcdir = \$(top_srcdir)/plugins/${pluginname}
.
#
# remove libtool line
/^LIBTOOL/d
#
# INCLUDES to _CPPFLAGS
g/^INCLUDES/s/INCLUDES/${pluginname}_la_CPPFLAGS/
#
#add srcdir to SOURCES
g/_la_SOURCES/s/\([^ ]*\.[^ ]*\)/\$(${pluginname}_la_srcdir)\/\1/g
#
#cxxflags
g/^AM_CXXFLAGS/s/AM_CXXFLAGS/${pluginname}_la_CXXFLAGS/
#
#add dir to HEADERS
g/_HEADERS/s/\([^ ]*\.[^ ]*\)/\$(${pluginname}_la_srcdir)\/\1/g
#
#add dir to backslashified lines
g/^	/s/\([^ 	]*\.[^ ]*\)/\$(${pluginname}_la_srcdir)\/\1/g
#
#add dir to EXTRA_DIST
g/EXTRA_DIST/s/\([^ ]*\.[^ ]*\)/\$(${pluginname}_la_srcdir)\/\1/g
#
# Fix LIBADD    libeffectv_la_LIBADD = \$(top_builddir)/plugins/colors/libcolors.la
g/_la_LIBADD/s/\(libeffectv_la_LIBADD\) *= *.*\/\([^/]*\)/\1 = \2/
#
# replace = with += for shared variables
g/^noinst_HEADERS/s/=/+=/
g/^noinst_LTLIBRARIES/s/=/+=/
g/^EXTRA_DIST/s/=/+=/
g/^plugin_LTLIBRARIES/s/=/+=/
wq
EOF
#end of edit script

        echo "Done"

        #diff -W200 -dy $makefileam $makefileam.new | less

        echo "End"
    fi
done
